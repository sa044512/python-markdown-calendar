# python-markdown-calendar-generator

## Purpose
This python module, which is also excutable, can be used to generated
Markdown syntax for producing a calendar. It is using the Markdown
table syntax for this. It can produce a single month calendar or a
calendar for a whole year. The year and month can be provided to
the module and on the commandline. If no year and month are provided
the current year and month are used.

As tables are not part of the original markdown syntax, your milage
may vary. The output works on [Github](https://www.github.com),
[GitLab](https://www.gitlab.com) and [hackmd.io](https://hackmd.io)

## Example Call
./MarkdownCalendar.py 1986 04

## Example Output

**April 1986**

| Mon | Tue | Wed | Thu | Fri | Sat | Sun |
|-----|-----|-----|-----|-----|-----|-----|
|     |   1 |   2 |   3 |   4 |   5 |   6 |
|   7 |   8 |   9 |  10 |  11 |  12 |  13 |
|  14 |  15 |  16 |  17 |  18 |  19 |  20 |
|  21 |  22 |  23 |  24 |  25 |  26 |  27 |
|  28 |  29 |  30 |     |     |     |     |

## Example Markdown

```
**April 1986**

| Mon | Tue | Wed | Thu | Fri | Sat | Sun |
|-----|-----|-----|-----|-----|-----|-----|
|     |   1 |   2 |   3 |   4 |   5 |   6 |
|   7 |   8 |   9 |  10 |  11 |  12 |  13 |
|  14 |  15 |  16 |  17 |  18 |  19 |  20 |
|  21 |  22 |  23 |  24 |  25 |  26 |  27 |
|  28 |  29 |  30 |     |     |     |     |
```

## CLI Parameters
```
Usage: MarkdownCalendar.py [options] [year [month]]

Options:
  -h, --help  show this help message and exit
```
## Installation
git clone https://gitlab.com/theomega/python-markdown-calendar.git
cd python-markdown-calendar

## Dependencies
Only python version 2 is supported currently.
